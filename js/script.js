$(function() {

  const menu = new MmenuLight(
    document.querySelector( "#menuMobile" ),
    "(max-width: 1200px)"
  );
  
  const navigator = menu.navigation();
  const drawer = menu.offcanvas();

  document.querySelector( '#burger' ).addEventListener( 'click', ( evnt ) => {
    
      evnt.preventDefault();
      drawer.open();
    });
   

    // sliders
    if(document.getElementById("hero")) {
      const hero = new Swiper('#hero .hero__slider', {
        loop: true,
        slidesPerView: 1,

        pagination: {
          el: '#hero .swiper-pagination',
          type: 'bullets',
          clickable: true
        },
      })

    }

    if(document.getElementById("geo")) {
      const geo = new Swiper('#geo .geo__slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 8,
        pagination: {
          el: '#geo .swiper-pagination',
          type: 'bullets',
          clickable: true
        }
      })
    }

    if (document.getElementById("client")) {
      const client = new Swiper('#client', {
        loop: true,
        slidesPerView: 1,
        breakpoints: {
          768: {
            slidesPerView: 2
          },
          992: {
            slidesPerView: 3
          },
          1400: {
            slidesPerView: 4
          }
        },
        pagination: {
          el: '#client .swiper-pagination',
          type: 'bullets',
          clickable: true
        },
      })
    }
    
    if (document.getElementById('reviews')) {
      const reviews = new Swiper('#reviews', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 0,
        breakpoints: {
          576: {
            slidesPerView: 2,
            spaceBetween: 15,
          },
          992: {
            slidesPerView: 3,
            spaceBetween: 15,
          },
          1400: {
            slidesPerView: 4,
            spaceBetween: 15,
          }
        },
        pagination: {
          el: '#reviews .swiper-pagination',
          type: 'bullets',
          clickable: true
        },
      })
    }

    if (document.getElementById('hero')) {

    }
    // const swiper = new Swiper('.swiper-container', {
    //   // Optional parameters
    //   loop: true,
    
    //   // If we need pagination
    //   pagination: {
    //     el: '.swiper-pagination',
    //   },
    
    //   // Navigation arrows
    //   navigation: {
    //     nextEl: '.swiper-button-next',
    //     prevEl: '.swiper-button-prev',
    //   },
    
    //   // And if we need scrollbar
    //   scrollbar: {
    //     el: '.swiper-scrollbar',
    //   },
    // });

    // color-picker the index.html/index.php on block direction

  

    if(document.querySelector('.direction .gradient') || document.querySelector('.direction .direction__image')) {
      var direction = $(".direction")
      
      direction.find('.direction__item').each(function() {
      })
    }

    if(document.getElementById('tel')) {
      $('#tel').keydown(function(e) {
        if(isNaN(+e.key) && e.key !== 'Backspace' || e.code === 'Space') {
          e.preventDefault()
        }
      })
      $('#tel').mask('+7 (999)-999-99-99');
    }
    
    if(document.getElementById('tab')) {
      function tabIdx(i) {
        $("#tab .tab__content").each(function(idx) {
          $(this).attr('data-hidden', i === idx ? '' : 'true' )
        })
      }

      function btnActive(i) {
        $('#tab .tab button').each(function(idx) {
          i === idx ? $(this).addClass("active") : $(this).removeClass("active");
        })
      }
      tabIdx(0)

      $('#tab .tab').on('click', function(e) {
        tabIdx(+$(e.target).attr('data-idx'))
        btnActive(+$(e.target).attr('data-idx'))
      })
    }
    
})